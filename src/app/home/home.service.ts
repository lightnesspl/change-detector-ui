import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

export interface PageDetails {
  id: string;
  title: string;
  url: string;
  keywords: string[];
  detectionStatus: string;
  frequency: string;
  nextDetection: string;
}

export class PageDetailsBody {
  title: string;
  url: string;
  keywords: string[];
  frequency: string;

  constructor(title: string, url: string, keywords: string[], frequency: string) {
    this.title = title;
    this.url = url;
    this.keywords = keywords;
    this.frequency = frequency;
  }

  public static empty() {
    return new PageDetailsBody('', '', [], 'WEEKLY');
  }
}

export interface PagePreview {
  content: string;
}

export class PagePreviewBody {
  url: string;

  constructor(url: string) {
    this.url = url;
  }
}

@Injectable()
export class HomeService {

  private pageDetailsListUrl = 'http://localhost:8080/page_details';
  private pagePreviewUrl = 'http://localhost:8080/page_preview';
  private pageDetailsUrl = 'http://localhost:8080/page_details';

  constructor(private http: HttpClient) {
  }

  public getCreatedPageDetails(): Observable<PageDetails[]> {
    return this.http.get<PageDetails[]>(this.pageDetailsListUrl);
  }

  public createPageDetails(pageDetailsAddBody: PageDetailsBody) {
    return this.http.post(this.pageDetailsUrl, pageDetailsAddBody);
  }

  public delete(id: string) {
    return this.http.delete(`${this.pageDetailsUrl}/${id}`);
  }

  public editPageDetails(id: string, pageDetailsEditBody: PageDetailsBody) {
    return this.http.put(`${this.pageDetailsUrl}/${id}`, pageDetailsEditBody);
  }

  public pagePreview(pageUrl: string) {
    const pagePreviewBody = new PagePreviewBody(pageUrl);
    return this.http.post<PagePreview>(this.pagePreviewUrl, pagePreviewBody);
  }
}
