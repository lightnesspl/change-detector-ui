import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {HomeService, PageDetails, PageDetailsBody} from './home.service';
import {AuthenticationService} from '../authentication.service';
import {Router} from '@angular/router';

@Component({
             selector: 'app-home',
             templateUrl: 'home.component.html',
             styleUrls: ['home.component.css'],
             providers: [AuthenticationService, HomeService],
             encapsulation: ViewEncapsulation.None
           })
export class HomeComponent implements OnInit {

  pageDetailsBody: PageDetailsBody = PageDetailsBody.empty();
  pageDetailsId: string;

  pageDetails: PageDetails[];
  pagePreview: string;
  editActive: boolean;
  addActive: boolean;

  constructor(private authenticationService: AuthenticationService, private router: Router, private homeService: HomeService) {
  }

  ngOnInit() {
    this.getCreatedPageDetails();
  }

  delete(pageDetail: PageDetails) {
    this.homeService.delete(pageDetail.id).subscribe(() => {
      this.getCreatedPageDetails();
    });
  }

  onEdit(pageDetailsId: string) {
    this.pageDetailsId = pageDetailsId;
    this.editActive = true;
    this.addActive = false;
  }

  editPageDetails(pageDetailsBody: PageDetailsBody) {
    this.homeService.editPageDetails(this.pageDetailsId, pageDetailsBody).subscribe(() => {
      this.pageDetailsId = null;
      this.getCreatedPageDetails();
      this.editActive = false;
      this.hidePreview();
    });
  }

  onCreate() {
    this.addActive = true;
    this.editActive = false;
  }

  createPageDetails(pageDetailsBody: PageDetailsBody) {
    this.homeService.createPageDetails(pageDetailsBody).subscribe(() => {
      this.pageDetailsBody = PageDetailsBody.empty();
      this.getCreatedPageDetails();
      this.addActive = false;
      this.hidePreview();
    });
  }

  cancel() {
    this.addActive = false;
    this.editActive = false;
    this.hidePreview();
  }

  showPagePreview(pageUrl: string) {
    this.homeService.pagePreview(pageUrl).subscribe(response => {
      this.pagePreview = response.content;
    });
  }

  hidePreview() {
    this.pagePreview = '';
  }

  getStatusColor(pageDetail: PageDetails) {
    switch (pageDetail.detectionStatus) {
      case ('FOUND'):
        return 'bg-green';
      case ('NOT_FOUND'):
        return 'bg-orange';
      case ('ERROR'):
        return 'bg-red';
      case ('UNAVAILABLE'):
        return 'bg-gray';
    }
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['login']);
  }

  private getCreatedPageDetails() {
    this.homeService.getCreatedPageDetails().subscribe(response => {
      this.pageDetails = response;
    });
  }
}
