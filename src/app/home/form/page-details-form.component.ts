import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';
import {PageDetails, PageDetailsBody} from '../home.service';

@Component({
             selector: 'app-page-details-form',
             templateUrl: 'page-details-form.component.html',
             encapsulation: ViewEncapsulation.None
           })
export class PageDetailsFormComponent implements OnInit {

  @Input()
  pageDetails: PageDetails;

  @Output()
  saved: EventEmitter<PageDetailsBody> = new EventEmitter<PageDetailsBody>();

  @Output()
  canceled: EventEmitter<any> = new EventEmitter<any>();

  @Output()
  pagePreview: EventEmitter<string> = new EventEmitter<string>();

  frequencies: any;

  constructor() {
  }

  ngOnInit() {
    this.frequencies = [
      {label: 'Once per week', value: 'WEEKLY'},
      {label: 'Once per day', value: 'DAILY'},
      {label: 'Once per hour', value: 'HOURLY'},
      {label: 'Every ten minutes', value: 'EVERY_TEN_MINUTES'},
      {label: 'Every minute', value: 'EVERY_ONE_MINUTE'},
    ];
  }

  onSaved() {
    this.saved.emit(new PageDetailsBody(this.pageDetails.title,
                                        this.pageDetails.url,
                                        this.pageDetails.keywords,
                                        this.pageDetails.frequency));
  }

  onCanceled() {
    this.canceled.emit();
  }

  onPagePreviewClicked() {
    this.pagePreview.emit(this.pageDetails.url);
  }
}
