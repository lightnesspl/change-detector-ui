import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

export class Authorization {
  token: string;
}

export class AuthorizationBody {
  username: string;
  password: string;

  constructor(username: string, password: string) {
    this.username = username;
    this.password = password;
  }
}

@Injectable()
export class AuthenticationService {

  private authUrl = 'http://localhost:8080/auth';

  constructor(private http: HttpClient) {
  }

  login(username: string, password: string): Observable<Authorization> {
    const authorizationBody = new AuthorizationBody(username, password);
    return this.http.post<Authorization>(this.authUrl, authorizationBody);
  }

  getToken(): String {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const token = currentUser && currentUser.token;
    return token ? token : '';
  }

  logout(): void {
    localStorage.removeItem('currentUser');
  }

  isLoggedIn(): boolean {
    const token: String = this.getToken();
    return token && token.length > 0;
  }
}
