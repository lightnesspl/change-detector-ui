import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../authentication.service';
import {Location} from '@angular/common';

@Component({
             templateUrl: 'login.component.html',
             styleUrls: ['login.component.css']
           })

export class LoginComponent implements OnInit {
  model: any = {};
  loading = false;
  error = '';

  constructor(private router: Router, private location: Location, private authenticationService: AuthenticationService) {
  }

  ngOnInit() {
    if (this.authenticationService.isLoggedIn()) {
      this.location.back();
    }
  }

  login() {
    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password)
      .subscribe(result => {
        localStorage.setItem('currentUser', JSON.stringify({username: this.model.username, token: result.token}));
        this.router.navigate(['home']);
      });
  }
}
